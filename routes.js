const nextRoutes = require('next-routes')
const routes = module.exports = nextRoutes()

routes.add('search', '/items', 'items')
routes.add('itemDetail', '/items/:id', 'detail')
routes.add('home', '/', 'index')
routes.add('category', '/category', 'category')
