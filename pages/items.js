import React from 'react'
import fetch from 'api/fetch'
import Page from '../components/Page'
import SearchList from '../components/SearchList'

export default class extends React.Component {
  static async getInitialProps ({ query }) {
    const { q } = query
    const { items, categories } = await fetch(q)
    return { items, categories, query: q }
  }

  render () {
    const { items, categories, query } = this.props

    return (
      <Page title={query} searchQuery={query} categories={categories} >
        <SearchList items={items} />
      </Page>
    )
  }
}
