import React from 'react'
import Router from 'next/router'
import Header from '../components/Header'
import Breadcrumb from '../components/Breadcrumb'
import Meta from '../components/Meta'

export default class extends React.Component {
  search (query) {
    Router.push(`/items?q=${query}`)
  }

  render () {
    return (
      <div>
        <Meta />
        <Header search={this.search} searchQuery='' />
        <div className='container'>
          <Breadcrumb categories={[]} />
          <div className='row'>
            <div className='col-lg-1 col-md-1' />
            <div className='col-lg-10 col-md-10'>
              <div className='row main'>
                Coming soon...
              </div>
            </div>

          </div>
        </div>
      </div>
    )
  }
}
