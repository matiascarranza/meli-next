import Page from '../components/Page'

export default () => (
  <Page title='MeLi Test' searchQuery='' categories={[]} >
    <div className='row'>
      <div className='col-lg-1 col-md-1 col-sm-1' />
      <div className='col-lg-10 col-md-10 col-sm-10'>
        <div className='row main'>
          Make some searches now...
        </div>
      </div>
    </div>
  </Page>
)
