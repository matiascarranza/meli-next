import React from 'react'
import { getDetail } from 'api/fetch'
import Page from '../components/Page'
import ItemDetail from '../components/ItemDetail'

export default class extends React.Component {
  static async getInitialProps ({ query }) {
    const { id } = query
    const { item } = await getDetail(id)
    return {
      item
    }
  }

  render () {
    const { item } = this.props

    return (
      <Page title={item.title} searchQuery='' categories={[]} >
        <div className='row'>
          <div className='col-lg-1 col-md-1 col-sm-1' />
          <div className='col-lg-10 col-md-10 col-sm-10'>
            <div className='row main-detail'>
              <ItemDetail item={item} />
            </div>
          </div>
        </div>
      </Page>
    )
  }
}
