import fetch from 'axios'

export default async (query) => {
  const result = await fetch(`https://meli-middleware-vcktgjmjjy.now.sh/api/items?q=${query}`)
  return result.data
}

export const getDetail = async (id) => {
  const result = await fetch(`https://meli-middleware-vcktgjmjjy.now.sh/api/items/${id}`)
  return result.data
}
