import currencyFormatter from 'currency-formatter'

import { Link } from '../routes'

export default ({ item }) => {
  const price = currencyFormatter.format(item.price.amount + '.' + item.price.decimals, {
    symbol: '$',
    decimal: '.',
    thousand: ',',
    decimalDigits: 2,
    format: '%s %v'
  })

  return (
    <div className='row item' key={item.id} >
      <div className='col-lg-3 col-md-3 col-sm-3 img-container'>
        <div className='thumbnail'>
          <Link prefetch route='itemDetail' params={{ id: item.id }}>
            <a><img alt='' src={item.picture || '/static/img/noimage.png'} width='180px' height='180px' /></a>
          </Link>
        </div>
      </div>
      <div className='col-lg-9 col-md-9 col-sm-9'>
        <div className='row info'>
          <div className='col-lg-10 col-md-10 col-sm-10 price_container'>
            {price}

            { item.free_shipping ? (
              <img className='free_shipping' src='/static/img/ic_shipping.png' />
            ) : '' }
          </div>
          <div className='col-lg-2 col-md-2 col-sm-2 state_name'>
            {item.address.city_name}
          </div>
        </div>
        <div className='row'>
          <div className='col-lg-8 col-md-8 col-sm-8 tite-container'>
            <h1 className='title'>
              <Link prefetch route='itemDetail' params={{ id: item.id }} >
                <a>{item.title}</a>
              </Link>
            </h1>
          </div>
        </div>
      </div>
    </div>
  )
}
