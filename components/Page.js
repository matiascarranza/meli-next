import Header from './Header'
import Meta from './Meta'
import Breadcrumb from './Breadcrumb'

export default ({ title, searchQuery, categories, children }) => (
  <div>
    <Meta title={title} />
    <Header searchQuery={searchQuery} />
    <div className='container'>
      <Breadcrumb categories={categories} />
      { children }
    </div>
  </div>
)
