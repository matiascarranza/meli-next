import React from 'react'
import stylesheet from 'styles/base.scss'
import Search from './Search'
import { Link } from '../routes'
import Router from 'next/router'

export default class extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      searchText: props.searchQuery,
      inStockOnly: false
    }
    this.handleChange = this.handleChange.bind(this)
    this.search = this.search.bind(this)
  }

  search (query) {
    Router.push(`/items?q=${query}`)
  }

  handleChange (value) {
    this.setState({searchText: value})
  }

  render () {
    return (
      <header>
        <style dangerouslySetInnerHTML={{ __html: stylesheet }} />
        <div className='header-meli'>
          <div className='container'>
            <div className='row'>
              <nav className='navbar navbar-default'>
                <div className='col-lg-1 col-md-1 col-sm-1' />
                <div className='col-lg-1 col-md-2 col-sm-2 navbar-header'>
                  <Link route='home'>
                    <a className='navbar-brand'>
                      <img alt='Brand' src='/static/img/Logo_ML.png' />
                    </a>
                  </Link>
                </div>

                <div className='col-lg-9 col-md-8 col-sm-8 search'>
                  <Search
                    searchText={this.state.searchText}
                    handleChange={this.handleChange}
                    onSearch={this.search} />
                </div>

                <div className='col-lg-1 col-md-1 col-sm-1' />
              </nav>
            </div>
          </div>
        </div>
      </header>
    )
  }
}
