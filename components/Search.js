import React from 'react'
import _ from 'lodash'

export default class extends React.Component {
  constructor (props) {
    super(props)
    this.handleFilterTextInputChange = this.handleFilterTextInputChange.bind(this)
    this.search = this.search.bind(this)
    this.searchInput = this.searchInput.bind(this)
  }

  search () {
    if (_.trim(this.props.searchText) !== '') {
      this.props.onSearch(this.props.searchText)
    }
  }

  searchInput (e) {
    if (e.key === 'Enter') {
      this.search()
    }
  }

  handleFilterTextInputChange (e) {
    this.props.handleChange(e.target.value)
  }

  render () {
    return (

      <div className='input-group'>
        <input
          value={this.props.searchText}
          onChange={this.handleFilterTextInputChange}
          ref='search'
          type='text'
          className='form-control'
          placeholder='Nunca dejes de buscar'
          onKeyPress={this.searchInput} />

        <span className='input-group-btn'>
          <button onClick={this.search} className='btn btn-default search-button' type='button'>
            <span className='glyphicon glyphicon-search' />
          </button>
        </span>
      </div>
    )
  }
}
