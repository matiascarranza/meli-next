import SearchItem from './SearchItem'
export default ({ items }) => (

  <div className='row'>
    <div className='col-lg-1 col-md-1 col-sm-1' />
    <div className='col-lg-10 col-md-10 col-sm-10'>
      <div className='row main'>
        { items.map((item, i) => (
          <SearchItem key={item.id} item={item} />
          ))
        }
      </div>
    </div>
  </div>

)
