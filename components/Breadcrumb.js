import React from 'react'
import { Link } from '../routes'

export default class extends React.Component {
  render () {
    const { categories } = this.props

    return (
      <div className='row'>
        <div className='col-lg-10 col-md-10 col-sm-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1'>
          <ol className='breadcrumb'>
            { categories.map((item, i) => (
              <li key={i}>
                <Link route='category' >
                  <a>{item}</a>
                </Link>
              </li>
            ))
          }
          </ol>
        </div>
      </div>
    )
  }
}
