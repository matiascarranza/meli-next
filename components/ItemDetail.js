import pluralize from 'pluralize'
import currencyFormatter from 'currency-formatter'

const conditions = {
  'new': 'Nuevo',
  'used': 'Usado'
}

export default ({ item }) => {
  const price = currencyFormatter.format(item.price.amount + '.' + item.price.decimals, {
    symbol: '$',
    decimal: '.',
    thousand: ',',
    decimalDigits: 2,
    format: '%s %v'
  })
  return (
    <div>
      <div className='row detail'>
        <div className='col-lg-7 col-md-7 col-sm-7 img-container__full'>
          <img className='img-responsive' alt='' src={item.picture || '/static/img/noimage.png'} />
        </div>
        <div className='col-lg-4 col-md-4 col-sm-4 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 shop-container'>
          <div className='row status_info'>
            <div className='col-lg-12 col-md-12 col-sm-12'>
              <span className='status'>{conditions[item.condition] || ''}</span> - {item.sold_quantity} {pluralize('vendido', item.sold_quantity)}
            </div>
          </div>
          <div className='row title'>
            <div className='col-lg-12 col-md-12 col-sm-12'>
              {item.title}
            </div>
          </div>
          <div className='row price'>
            <div className='col-lg-12 col-md-12 col-sm-12'>
              <span className='price'>
                {price}
              </span>
              { item.free_shipping ? (
                <img className='free_shipping' src='/static/img/ic_shipping.png' />
              ) : '' }
            </div>
          </div>
          <div className='row'>
            <div className='col-lg-12 col-md-12 col-sm-12 shop-button-container'>
              <button
                type='button'
                name='button'
                className='btn btn-primary btn-lg btn-block shop-button'>
                  Comprar
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className='row item-description'>
        <div className='col-lg-7 col-md-7 col-sm-7'>
          <h3>Descripcion del Producto</h3>
          <p>{item.description}</p>
        </div>
      </div>
    </div>
  )
}
