/* eslint-env jest */

import Home from '../../pages/home.js'
import React from 'react'
import { shallow } from 'enzyme'

describe('Home page', () => {
  it('has a title', () => {
    const app = shallow(<Home />)

    expect(app.find('div.row.main').text()).toEqual('Make some searches now...')
  })
})
