/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'

import Header from '../../components/Header.js'
import Search from '../../components/Search.js'

describe('Header Component', () => {
  it('Renders a Header with some seach', () => {
    const header = shallow(<Header searchQuery='some query' />)

    expect(header.find(Search).length).toEqual(1)
    expect(header.find(Search).prop('searchText')).toEqual('some query')
  })

  it('Renders a Header without seach', () => {
    const header = shallow(<Header searchQuery='' />)

    expect(header.find(Search).length).toEqual(1)
    expect(header.find(Search).prop('searchText')).toEqual('')
  })
})
