/* eslint-env jest */

import Breadcrumb from '../../components/Breadcrumb.js'
import React from 'react'
import { shallow } from 'enzyme'

describe('Breadcrumb Component', () => {
  it('Renders a Breadcrumb with one item', () => {
    const breadcrumb = shallow(<Breadcrumb categories={['one']} />)

    expect(breadcrumb.find('li').length).toEqual(1)
    expect(breadcrumb.find('li a').text()).toEqual('one')
  })

  it('Renders a Breadcrumb with no items', () => {
    const breadcrumb = shallow(<Breadcrumb categories={[]} />)

    expect(breadcrumb.find('li').length).toEqual(0)
  })
})
