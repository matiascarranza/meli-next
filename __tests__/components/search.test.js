/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'

import Search from '../../components/Search.js'

describe('Search Component', () => {
  it('Renders a Search with some seach query', () => {
    const search = shallow(<Search searchText='some query' />)

    expect(search.find('input').length).toEqual(1)
    expect(search.find('input').props().value).toEqual('some query')
  })

  it('Performs a search when button is clicked', () => {
    const onSearch = jest.fn()

    const searchComponent = shallow(<Search searchText='auto' onSearch={onSearch} />)

    searchComponent.find('button').simulate('click')
    expect(onSearch).toBeCalledWith('auto')
  })

  it('Doesnt perform a search when button is clicked without query', () => {
    const onSearch = jest.fn()

    const searchComponent = shallow(<Search searchText='' onSearch={onSearch} />)

    searchComponent.find('button').simulate('click')
    expect(onSearch).not.toBeCalled()
  })

  it('Doesnt perform a search when button is clicked without query', () => {
    const onSearch = jest.fn()

    const searchComponent = shallow(<Search searchText='pepe' onSearch={onSearch} />)

    searchComponent.find('input').simulate('keyPress', { key: 'Enter' })
    expect(onSearch).toBeCalledWith('pepe')
  })

  it('Doesnt perform a search when button is clicked without query', () => {
    const onSearch = jest.fn()

    const searchComponent = shallow(<Search searchText='' onSearch={onSearch} />)

    searchComponent.find('input').simulate('keyPress', { key: 'Enter' })
    expect(onSearch).not.toBeCalled()
  })
})
